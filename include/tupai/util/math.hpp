/*
* 	file : math.hpp
*
* 	This file is part of Tupai.
*
* 	Tupai is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Tupai is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Tupai.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TUPAI_UTIL_MATH_HPP
#define TUPAI_UTIL_MATH_HPP

// Tupai
#include <tupai/type.hpp>

namespace tupai
{
	namespace util
	{
		template<typename T, typename V> T max(T a, V b) { return (a > b) ? a : b; }
		template<typename T, typename V> T min(T a, V b) { return (a < b) ? a : b; }
		template<typename T> T sign(T x) { return (x > 0) ? 1 : ((x < 0) ? -1 : 0); }
		template<typename T> T abs(T x) { return (x < 0) ? -x : x; }
	}
}

#endif
